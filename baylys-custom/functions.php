<?php
/**
 * Baylys custom functions
 *
 * @package baylys
 *
 */


/*
 * =====================================================================
 *
 * Plugin Jetpack: Infinite Scroll
 *
 * http://jetpack.me/support/infinite-scroll/
 *
 * =====================================================================
 */

/*
 * Do we have footer widgets?
 *
 * @return bool
 */
function baylys_custom_further_has_footer_widgets( $has_widgets ) {
  if (
    ( ( Jetpack_User_Agent_Info::is_ipad() || ( function_exists( 'jetpack_is_mobile' ) && jetpack_is_mobile() ) ) && is_active_sidebar( 'sidebar-1' ) ) ||  // 'Main Sidebar'
    is_active_sidebar( 'sidebar-2' ) ||     // 'Footer Column 1'
    is_active_sidebar( 'sidebar-3' ) ||     // 'Footer Column 2'
    is_active_sidebar( 'sidebar-4' ) ||     // 'Footer Column 3'
    is_active_sidebar( 'sidebar-5' )        // 'Footer Column 4'
  )
    return true;

  return $has_widgets;
}
add_filter( 'infinite_scroll_has_footer_widgets', 'baylys_custom_further_has_footer_widgets' );

/*
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function baylys_custom_setup() {

  add_theme_support( 'infinite-scroll', array(
    'container' => 'content',
    'footer' => 'main-wrap',
  ) );

}
add_action( 'after_setup_theme', 'baylys_custom_setup' );
