# "Baylys" child theme for WordPress #

WordPress child theme for "Baylys" by [Elmastudio](http://www.elmastudio.de/).

![screenshot.png](https://bytebucket.org/MarkyMarkNo1/wp-theme-baylys-child/raw/43c83e28acadecbd222ad695f87b821c9c20a66f/baylys-custom/screenshot.png)

## Parent theme ##

* ["Baylys" homepage](http://www.elmastudio.de/wordpress-themes/baylys/)
* ["Baylys" demo](http://themes.elmastudio.de/baylys/)

## Child theme enhancements ##

* Jetpack plugins' [Infinite Scroll](http://jetpack.me/support/infinite-scroll/) support
* General layout improvements

## About WordPress ##

* [WordPress child themes](http://codex.wordpress.org/Child_Themes)
* [WordPress.org](http://wordpress.org/)
